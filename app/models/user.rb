class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  before_validation :set_password

  has_many :users_games
  has_many :games, through: :users_games
  has_many :creators_games
  has_many :own_games, through: :creators_games, source: :game

  def set_password
    generated_password = Devise.friendly_token.first(8)
    self.password ||= generated_password
    self.password_confirmation ||= password
  end
end
