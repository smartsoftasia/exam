class DashboardController < ApplicationController
  def index
  end

  def exam_1
  end

  def get_prime
    ### exam_1 ####
    @result = nil
  end

  def exam_2
  end

  def get_user_game
    ### exam_2                          ###
    ### u can send more global variable ###
    @result = User.new
  end

  def exam_3
  end

  def get_user_game_hard
    ### exam_3                            ###
    ### return user                       ###
    ### Can't add more global variable    ###
    ### Try to make everything in 1 query ###
    @result = User.new
  end

  def exam_4
    ### exam 4                                                            ###
    ### your have to set current_user as a user that have at least 1 game ###
    ### You can change global variable                                    ###
    @current_user = nil ### have to change by random with at least 1 game ###
    @games = Game.all
  end

  def exam_5
    ### exam 5                                                            ###
    ### your have to set current_user as a user that have at least 1 game ###
    ### You can change global variable                                    ###
    @current_user = nil ### have to change by random with at least 1 game ###
    @games = Game.all
  end

  def exam_6
    @games = Game.limit(5)
  end

  def exam_7
    @games = []
  end

  def exam_8
    @games = Game.limit(5)
  end

  def exam_9
    @games = []
  end

  def exam_10
    @games = Game.all
  end
end
